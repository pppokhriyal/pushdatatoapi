#!/usr/bin/env python

import sqlite3
import threading
import json
import requests
import time
from datetime import datetime
from time import sleep
import configparser


"""push data to api """
def push_data(rec_data,idrow):
    try:
        con = get_conn()
        cur = con.cursor()
        headers = {'Content-Type': 'application/json'}
        response = requests.post(apiurl,data=json.dumps(rec_data),headers=headers,timeout=apiurl_timeout)
        
        if client == "dolphin":
            rscode="res0000"
            if response.status_code == 200 and response.json()['resCode'] == rscode:
                for i in idrow:
                    cur.execute("UPDATE customstorage SET flag=1 WHERE rowid=?",(i,))
                    con.commit()

                cur.close()
                cur.close()
            else:
                print "No response from API"
        else:
            if response.status_code==201 or response.status_code==200:
                for i in idrow:
                    cur.execute("UPDATE customstorage SET flag=1 WHERE rowid=?",(i,))
                    con.commit()

                cur.close()
                con.close()
            else:
                print "No response from TrackRF API"

    except Exception,e:
        print e

def get_data():

    def dict_factory(cursor,row):
        d = {}
        for idx,col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    """convert datetime to timestamp"""
    def getimestamp(reqdatetime):
        dstring = reqdatetime
        date_element = datetime.strptime(dstring,"%d-%m-%Y %H:%M:%S %p")
        date_tuple = element.timetuple()
        timestamp = time.mktime(date_tuple)
        return int(timestamp)

    while True:
        """check if it is custom data format"""
        if client == "dolphin":
            
            """ Example Data format
               {"imei":"00142D619D22","data":[{"DETECTED_DATE_TIME":"1559532928","LONGITUDE":"0.0","TAG_ID":"313030303120202020202020","LATTITUDE":"0.0"}],"tableName":"DUMP_DATATRAP_ANDROID"} 
            """
            parent_dict = {}
            child_dict  = {}
            datalist    = []
            child_dictlist = []
            
            con = get_conn()
            con.row_factory = dict_factory
            cur = con.cursor()

            #check if row count is empty
            cur.execute("SELECT COUNT(*) FROM customstorage")
            result_cur=cur.fetchall()

            if result_cur[0].values()[0] > 0:

                cur.execute("SELECT rowid,epc_id,timestamp FROM customstorage WHERE flag=0 LIMIT 5")
                row_list = [i['rowid'] for i in cur.fetchall()]
                cur.execute("SELECT rowid,epc_id,timestamp FROM customstorage WHERE flag=0 LIMIT 5")
                data_result = cur.fetchall()

                for i in data_result:
                    child_dict["TAG_ID"] = i["epc_id"].encode('ascii')
                    child_dict["DETECTED_DATE_TIME"] = str(getimestamp(i["timestamp"].encode('ascii')))
                    child_dict["LATTITUDE"] = "0.0"
                    child_dict["LONGITUDE"] = "0.0"
                    child_dictlist.append(child_dict)

                parent_dict["tableName"] = tablename
                parent_dict["imei"] = readerid
                parent_dict["data"] = child_dictlist

                push_data(parent_dict,row_list)
                cur.close()
                con.close()
            else:
                cur.close()
                con.close()

            sleep(1)    
        else:
            print "TrackRFID"
            con = get_conn()
            con.row_factory = dict_factory
            cur = con.cursor()

            #check if row count is empty
            cur.execute("SELECT COUNT(*) FROM customstorage")
            result_cur=cur.fetchall()

            if result_cur[0].values()[0] > 0:
                cur.execute("SELECT rowid,reader_id,epc_id,tag_id,usr_id,timestamp,tag_rssi FROM customstorage WHERE flag=0 LIMIT 150")
                row_list = [i['rowid'] for i in cur.fetchall()]
                cur.execute("SELECT rowid,reader_id,epc_id,tag_id,usr_id,timestamp,tag_rssi FROM customstorage WHERE flag=0 LIMIT 150")

                data_result = cur.fetchall()
                push_data(data_result,row_list)

                cur.close()
                con.close()
            else:
                con.close()
                con.close()

            sleep(1)


def get_conn():
    conn = sqlite3.connect('/usr/local/db/database.db',check_same_thread=False)
    return conn

def main():
    t = threading.Thread(target=get_data)
    t.start()

if __name__ == '__main__':

    """read api configuration ini"""
    config = configparser.ConfigParser()
    config.read('/etc/uhf/reader.ini')

    apiurl=str(config['Reader']['apiurl'])
    apiurl_timeout = int(config['Reader']['apitimeout'])
    readerid = str(config['Reader']['readerid'])
    client = str(config['Reader']['client'])
    tablename = str(config['Reader']['table'])

    """start main thread"""
    main()


